package com.xerox.rramobile;
public class Account {
    int Account_ID;
    String AccountName;
    String Region;
    String Operation;
    String ClientName;

    public Account(int i, String AccountName, String Region, String Operation, String ClientName) {
        super();
        this.Account_ID = i;
        this.AccountName = AccountName;
        this.Region = Region;
        this.Operation = Operation;
        this.ClientName = ClientName;
    }

    public int getId() {
        return Account_ID;
    }

    public void setId(int Account_ID) {
        this.Account_ID = Account_ID;
    }

    public String getCountryName() {
        return AccountName;
    }

    public void setCountryName(String AccountName) {
        this.AccountName = AccountName;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion (String Region) {
        this.Region = Region;
    }

    public String getOperation() {
        return Operation;
    }

    public void setOperation(String Operation) {
        this.Operation = Operation;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String ClientName) {
        this.ClientName = ClientName;
    }
}
