package com.xerox.rramobile;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAccountList extends BaseAdapter {


    private Activity context;
    ArrayList<Account> countries;


    public CustomAccountList(Activity context, ArrayList<Account> countries) {
     //   super(context, R.layout.row_item, countries);
        this.context = context;
       this.countries=countries;

    }

    public static class ViewHolder
    {
        TextView textViewAccountName;
        TextView textViewClientName;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;

        LayoutInflater inflater = context.getLayoutInflater();
        ViewHolder vh;
        if(convertView==null) {
            vh=new ViewHolder();
            row = inflater.inflate(R.layout.row_item, null, true);
            vh.textViewAccountName = (TextView) row.findViewById(R.id.textViewAccountName);
            vh.textViewClientName = (TextView) row.findViewById(R.id.textViewClientName);
            // store the holder with the view.
            row.setTag(vh);
        }
        else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh.textViewClientName.setText(countries.get(position).getClientName());
        vh.textViewAccountName.setText(""+countries.get(position).getCountryName());

        return  row;
    }

    public long getItemId(int position) {
        return position;
    }

    public Object getItem(int position) {
        return position;
    }

    public int getCount() {

        if(countries.size()<=0)
            return 1;
        return countries.size();
    }
}

