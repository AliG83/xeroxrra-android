package com.xerox.rramobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    private Button btnSubmit;
    String responseText;
    StringBuffer response;
    URL url;
    Activity activity;
    ArrayList<Account> countries=new ArrayList<Account>();
    private ProgressDialog progressDialog;
    ListView listView;

      // In case if you deploy rest web service, then use below link and replace below ip address with yours
    //http://192.168.2.22:8080/JAXRSJsonExample/rest/countries
    
    //Direct Web services URL
    //private String path = "http://13.129.89.31:8080/DynaREST2_0/api/GridData?SetupTable=SuperUser&UserId=30092580&ProjectName=TLM&Filter=NULL&filterscount=0&groupscount=0&sortorder=&pagenum=0&pagesize=10&recordstartindex=0&recordendindex=18&_=1513263711338";
    //private String path = "http://10.72.104.215/RestFulServiceTest/api/Test";
    private String path = "http://theapparchitect.net/api/Test.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            activity = this;
            btnSubmit = (Button) findViewById(R.id.btnSubmit);
            listView = (ListView) findViewById(android.R.id.list);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    countries.clear();
                    //Call WebService
                    new GetServerData().execute();
                }
            });
        }

        class GetServerData extends AsyncTask
        {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                // Showing progress dialog
                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage("Fetching Accounts");
                progressDialog.setCancelable(false);
                progressDialog.show();

            }

            @Override
            protected Object doInBackground(Object[] objects) {
                return getWebServiceResponseData();
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                // Dismiss the progress dialog
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                // For populating list data
                CustomAccountList customAccountList = new CustomAccountList(activity, countries);
                listView.setAdapter(customAccountList);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        //Toast.makeText(getApplicationContext(),"You Selected "+countries.get(position).getCountryName()+ "",Toast.LENGTH_SHORT).show();
                        String passedAccountName = countries.get(position).getCountryName();
                        String passedRegion = countries.get(position).getRegion();
                        String passedOperation = countries.get(position).getOperation();
                        Intent intent = new Intent(MainActivity.this, DetailsViewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("accountName", passedAccountName);
                        bundle.putString("region", passedRegion);
                        bundle.putString("operation", passedOperation);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        }
        protected Void getWebServiceResponseData() {

            try {

                url = new URL(path);
                Log.d(TAG, "ServerData: " + path);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(150000);
                conn.setConnectTimeout(150000);
                conn.setRequestMethod("GET");

                int responseCode = conn.getResponseCode();

                Log.d(TAG, "Response code: " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    // Reading response from input Stream
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    String output;
                    response = new StringBuffer();

                    while ((output = in.readLine()) != null) {
                        response.append(output);
                    }
                    in.close();
                }}
             catch(Exception e){
                    e.printStackTrace();
            }

                responseText = response.toString();
                //Call ServerData() method to call webservice and store result in response
              //  response = service.ServerData(path, postDataParams);
                Log.d(TAG, "data:" + responseText);
                try {
                    JSONArray jsonarray = new JSONArray(responseText);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        int id = jsonobject.getInt("Account_ID");
                        String country = jsonobject.getString("Account Name");
                        String region = jsonobject.getString("Region");
                        String operation = (jsonobject.getString("Operation"));
                        String clientName = (jsonobject.getString("Client Name1"));
                        String CapitalizedCountry = country.substring(0, 1).toUpperCase() + country.substring(1).toLowerCase();
                        Account accountObj =new Account(id,CapitalizedCountry,region,operation,clientName);
                        countries.add(accountObj);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
    }
