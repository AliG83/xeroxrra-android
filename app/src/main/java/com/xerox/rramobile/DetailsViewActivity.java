package com.xerox.rramobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_view);

        Bundle bundle = getIntent().getExtras();
        String receivedAccountName = bundle .getString("accountName");
        String receivedRegion = bundle .getString("region");
        String receivedOperation = bundle .getString("operation");

        TextView accountNameTextView = (TextView) findViewById(R.id.accountName);
        TextView regionNameTextView = (TextView) findViewById(R.id.regionName);
        TextView operationNameTextView = (TextView) findViewById(R.id.operationName);

        accountNameTextView.setText((CharSequence) receivedAccountName);
        regionNameTextView.setText((CharSequence) receivedRegion);
        operationNameTextView.setText((CharSequence) receivedOperation);

    }
}
